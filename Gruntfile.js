module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: {
            files: ['Gruntfile.js', 'client/app/**/*.js'],
            options: {
                globals: {
                    jQuery: true
                }
            }
        },
        nodemon: {
            dev: {
                script: './server/app.js'
            }
        }

    });
    grunt.loadNpmTasks('grunt-nodemon');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.registerTask('default', ['jshint']);
    grunt.registerTask('serve',  ['jshint', 'nodemon']);
};