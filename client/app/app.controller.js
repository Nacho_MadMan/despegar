(function() {
    'use strict';
    angular.module('despegarApp')
        .controller('AppController', AppController);

    AppController.$inject = ['hotels', 'lists', 'comments', 'ServiceHelperDataStore'];

    /**
     * @name           AppController
     * @description    Main Controller for the app.
     * @param {Object} hotels - resolve promise of the service hotels (*) response.
     * @param {Object} lists - resolve promise of the service lists (*) response.
     * @param {Object} comments - resolve promise of the service comments (*) response.
     * @param {Object} ServiceHelperDataStore - helper class to store resolved data to be used outside this controller.
     */
    function AppController(hotels, lists, comments, ServiceHelperDataStore) {
        var vm = this;
        activate();

        /**
         * @name activate
         * @description activate method for AppController.
         */
        function activate() {
            vm.hotels = hotels;
            vm.lists = lists.list;
            vm.comments = comments;
            vm.footerLists = lists.footerInfoList;
            ServiceHelperDataStore.setData({hotels: hotels, lists: lists, comments: comments});
        }
    }
})();