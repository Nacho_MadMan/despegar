(function() {
    'use strict';
    angular.module('listCard')
        .controller('ListCardController', ListCardController);

    ListCardController.$inject = ['$element'];
    /**
     * @name                ListCardController
     * @description         Main controller for listcard directive.
     */
    function ListCardController($element) {
        var busy = false;
        /**
         * bind click event to availability button and add the handler to display the availability message
         */
        $element.find('#availabilityButton')
            .on('click', AvailabilityHandler);

        /**
         * @name            AvailabilityHandler
         * @description     Handler that controls the availability message.
         */
        function AvailabilityHandler() {
            if (!busy) {
                var activeBanner = angular.element('.availabilityBanner.active'),
                    $banner = $element.find(".availabilityBanner"),
                    options = {
                        opacity: 0.5,
                        width: $banner.css('width') === '237px' ? "0" : '+=237'
                    };
                busy = true;
                if (activeBanner.length > 0) {
                    activeBanner.removeClass('active');
                    activeBanner.animate({width: 0}, 500);
                }
                $banner.animate(options, 500, function() {
                    busy = false;
                    angular.element(this).addClass('active');
                });
            }
        }
    }
})();