(function(){
    'use strict';
    angular.module('listCard')
        .directive('listCard', ListCardDirective);

    /**
     * @name                     ListCardDirective
     * @description              List card directive.
     * @returns {*}
     */
    function ListCardDirective(){
        return {
            restrict: 'E',
            controller: 'ListCardController',
            controllerAs: 'listCardVm',
            templateUrl: 'components/listCard/listCard.template.html'
        };
    }
})();
