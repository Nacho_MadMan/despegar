(function(){
    'use strict';
    angular.module('hotelCategories')
        .directive('hotelCategories', HotelCategoriesDirective);

    /**
     * @name                HotelCategoriesDirective
     * @description         hotel categories directive.
     * @returns {*}
     */
    function HotelCategoriesDirective() {
        return {
            restrict: 'E',
            templateUrl: 'components/hotelCategories/hotelCategories.template.html'
        };
    }
})();
