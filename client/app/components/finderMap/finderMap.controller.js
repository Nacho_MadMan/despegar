(function() {
    'use strict';
    angular.module('finderMap')
        .controller('FinderMapController', FinderMapController);

    FinderMapController.$inject = ['FinderMapFactory'];

    /**
     * @name                                FinderMapController
     * @description                         finderMap component controller
     * @param {Object} FinderMapFactory     finderMap component factory
     */
    function FinderMapController(FinderMapFactory) {
        /**
         * init the map from the factory.
         */
        FinderMapFactory.init();
    }
})();