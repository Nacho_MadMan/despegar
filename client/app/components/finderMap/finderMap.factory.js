(function() {
    'use strict';
    angular.module('finderMap')
        .factory('FinderMapFactory', FinderMapFactory);

    FinderMapFactory.$inject = ['GoogleMapsFactory', 'ServiceHelperDataStore'];

    /**
     * @name                                FinderMapFactory
     * @param {Object} GoogleMapsFactory    Google maps wrapper
     * @returns {*}
     */
    function FinderMapFactory(GoogleMapsFactory, ServiceHelperDataStore) {
        var mapMarker = 'media/images/mapPin.png',
            serviceResponse = ServiceHelperDataStore.getData();

        return {
            init: init
        };

        /**
         * @name                       getMapOptions
         * @description                Get google map options object.
         * @returns {*}
         */
        function getMapOptions() {
            return {
                center: new GoogleMapsFactory.google.maps.LatLng(-22.9039800, -43.2069000),
                zoom: 15,
                disableDefaultUI: true,
                mapTypeId: GoogleMapsFactory.google.maps.MapTypeId.ROADMAP
            };
        }

        /**
         * @name                       init
         * @description                Initialize google map.
         */
        function init() {
            var mapCanvas = document.getElementById('map-canvas'),
                map = new GoogleMapsFactory.google.maps.Map(mapCanvas, getMapOptions());
            return createMapMarkers(serviceResponse, map);
        }

        /**
         * @name                       createMapMarkers
         * @description                create map markers
         * @param {Object} response
         * @param {Object} map
         */
        function createMapMarkers(response, map) {
            response.hotels.forEach(function(hotel) {
                new GoogleMapsFactory.google.maps.Marker({
                    position: new GoogleMapsFactory
                        .google.maps.LatLng(hotel.geoLocation.lat, hotel.geoLocation.long),
                    map: map,
                    icon: mapMarker,
                    title: hotel.name
                }).setMap(map);
            });
        }
    }
})();