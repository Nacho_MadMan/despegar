(function(){
    'use strict';
    angular.module('finderMap')
        .directive('finderMap', FinderMapDirective);

    /**
     * @name              FinderMapDirective
     * @description       finder map directive.
     * @returns {*}
     */
    function FinderMapDirective(){
        return {
            restrict: 'E',
            controller: 'FinderMapController',
            templateUrl: 'components/finderMap/finderMap.template.html'
        };
    }
})();
