(function(google){
    'use strict';
    angular.module('googleMap')
        .factory('GoogleMapsFactory', GoogleMapsFactory);
    /**
     * Wrapper class to inject google api into angular project. To avoid using global variables to call the api into
     * the controllers or factories.
     */
    function GoogleMapsFactory(){
        return {
            google: google
        };
    }
})(google);