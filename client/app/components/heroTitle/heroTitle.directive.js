(function(){
    'use strict';
    angular.module('heroTitle')
        .directive('heroTitle', HeroTitleDirective);
    /**
     * @name               HeroTitleDirective
     * @description        hero title directive.
     * @returns {*}
     */
    function HeroTitleDirective() {
        return {
            restrict: 'E',
            templateUrl: 'components/heroTitle/heroTitle.template.html'
        };
    }
})();
