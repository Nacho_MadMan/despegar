(function(){
    'use strict';
    angular.module('despegarApp.components', [
        'hotelFinder',
        'listCard',
        'commentBubble',
        'hotelCategories',
        'infoList',
        'finderMap',
        'paginator',
        'promoDisplay',
        'socialModule',
        'serviceDataStore',
        'breadcrums',
        'heroTitle',
        'filters'
    ]);
})();