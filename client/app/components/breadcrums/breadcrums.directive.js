(function(){
    'use strict';
    angular.module('breadcrums')
        .directive('breadcrums', BreadCrumsDirective);

    /**
     * @name                BreadCrumDirective
     * @description         Breadcrums directive component.
     */
    function BreadCrumsDirective(){
        return {
            restrict: 'E',
            templateUrl: 'components/breadcrums/breadcrums.template.html'
        };
    }
})();