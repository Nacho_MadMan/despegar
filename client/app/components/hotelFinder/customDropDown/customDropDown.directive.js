(function() {
    'use strict';
    angular.module('customDropDown')
        .directive('dropDown', DropDownDirective);

    /**
     * @name                             dropDownDirective
     * @description                      Custom dropdown directive
     * @returns {*}
     */
    function DropDownDirective() {
        return {
            restrict: 'E',
            scope: {
                items: '=',
                label: '=',
                model: '=',
                value: '='
            },
            controller: 'CustomDropDownController',
            templateUrl: 'components/hotelFinder/customDropDown/customDropDown.template.html'
        };
    }
})();