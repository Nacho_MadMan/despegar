(function() {
    'use strict';
    angular.module('customDropDown')
        .controller('CustomDropDownController', CustomDropDownController);

    CustomDropDownController.$inject = ['$scope', '$rootScope', '$window', '$timeout'];

    /**
     * @name                     CustomDropDownController
     * @description              main controller for custom dropdown directive
     * @param $scope             scope provider
     * @param $rootScope         root scope provider
     * @param $window            window provider
     * @param $timeout           timeout provider
     */
    function CustomDropDownController($scope, $rootScope, $window, $timeout) {
        /**
         * @var                    menuOn
         * @description            flag to store the state of the dropdown
         * @type {boolean}
         */
        $scope.menuOn = false;

        /**
         * @name                   closeOpenedDropDowns
         * @description            Close any existent opened dropdowns.
         */
        function closeOpenedDropDowns() {
            var openedDropDown = angular.element('drop-down .sprite.sprite-dropDown.open'),
                scope;
            if (openedDropDown.length > 0) {
                scope = openedDropDown.scope();
                scope.menuOn = false;
                $timeout(function() {
                    scope.$apply();
                }, 1);
                openedDropDown.removeClass('open');
            }
        }

        /**
         * @name                   toggleMenu
         * @description            toggles menu flag on/off
         * @param event
         */
        $scope.toggleMenu = function(event) {
            closeOpenedDropDowns();
            $scope.menuOn = !($scope.menuOn);
            angular.element(event.target).addClass('open');
            event.stopPropagation();
        };

        /**
         * @name                   getValue
         * @description            Gets the value from the dropdown and emits the event to be listened
         * @param value
         */
        $scope.getValue = function(value) {
            var payload;
            $scope.value = value;
            payload = {
                key: $scope.model,
                value: $scope.value
            };
            $rootScope.$broadcast('dropDownItemSelected', payload);
        };

        /**
         * Toggles the menu off if clicking outside the dropdown
         */
        angular.element($window).click(function() {
            if ($scope.menuOn) {
                $scope.menuOn = false;
                $scope.$apply();
            }
        });
    }
})();
