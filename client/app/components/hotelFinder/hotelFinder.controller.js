(function() {
    'use strict';
    angular.module('hotelFinder')
        .controller('HotelFinderController', HotelFinderController);

    HotelFinderController.$inject = ['$rootScope'];

    /**
     * @name                     HotelFinderController
     * @description              Main controller for hotelFinder directive.
     * @param $rootScope
     */
    function HotelFinderController($rootScope) {
        var vm = this;

        /**
         * @name             vm.schema
         * @description      hotel search form schema
         * @type {*}
         */
        vm.schema = {
            city: '',
            checkIn: '',
            checkOut: '',
            quantity: '',
            guest: {
                adults: 0,
                minors: 0
            }
        };

        /**
         * Dummy method to simulate the form submit
         */
        vm.send = function(){
            console.log('DATA TO BE SENT:', vm.schema);
        };

        /**
         * listen to the custom dropdown directive broadcast event
         */
        $rootScope.$on('dropDownItemSelected', function(event, data) {
            vm.schema.guest[data.key] = data.value;
        });
    }
})();