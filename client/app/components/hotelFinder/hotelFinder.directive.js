(function() {
    'use strict';
    angular.module('hotelFinder')
        .directive('hotelFinder', HotelFinderDirective);

    /**
     * @name                      HotelFinderDirective
     * @description               Hotel Finder directive.
     * @returns {*}
     */
    function HotelFinderDirective() {
        return {
            restrict: 'E',
            controller: 'HotelFinderController',
            controllerAs: 'hotelFinderVm',
            templateUrl: 'components/hotelFinder/hotelFinder.template.html'
        };
    }
})();