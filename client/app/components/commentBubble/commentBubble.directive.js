(function(){
    'use strict';
    angular.module('commentBubble')
        .directive('commentBubble', CommentBubbleDirective);

    /**
     * @name                  CommentBubbleDirective
     * @description           comment bubble directive.
     * @returns {*}
     */
    function CommentBubbleDirective(){
        return {
            restrict: 'E',
            templateUrl: 'components/commentBubble/commentBubble.template.html'
        };
    }
})();
