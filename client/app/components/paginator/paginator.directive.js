(function(){
    'use strict';
    angular.module('paginator')
        .directive('paginator', PaginatorDirective);

    /**
     * @name                  PaginatorDirective
     * @description           directive for paginator.
     * @returns {*}
     */
    function PaginatorDirective(){
        return {
            restrict: 'E',
            controller: 'PaginatorController',
            controllerAs: 'paginatorVm',
            templateUrl: 'components/paginator/paginator.template.html'
        };
    }
})();
