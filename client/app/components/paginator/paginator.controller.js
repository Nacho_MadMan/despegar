(function(){
    'use strict';
    angular.module('paginator')
        .controller('PaginatorController', PaginatorController);

    PaginatorController.$inject=['$scope'];

    /**
     * @name              PaginatorController
     * @description       Pagination controller.
     * @param $scope
     */
    function PaginatorController($scope){

        var vm = this;

        $scope.appVm.itemsPerPage = vm.itemsPerPage = 5;
        $scope.appVm.currentPage = vm.currentPage = 0;

        /**
         * @name pageCount
         * @description get total pages.
         * @returns {number}
         */
        vm.pageCount = function() {
            return Math.ceil($scope.appVm.hotels.length / vm.itemsPerPage);
        };

        /**
         * @name range
         * @description determines the range of pages to be filtered.
         * @returns {Array}
         */
        vm.range = function() {
            var ret = [],
                i;
            for (i = 0; i < vm.pageCount(); i++) {
                ret.push(i);
            }
            return ret;
        };

        /**
         * @name prevPage
         * @description gets prev page.
         */
        vm.prevPage = function() {
            if (vm.currentPage > 0) {
                vm.currentPage--;
                $scope.appVm.currentPage = vm.currentPage;
            }
        };

        /**
         * @name prevPageDisabled
         * @description disable prev
         * @returns {string}
         */
        vm.prevPageDisabled = function() {
            return vm.currentPage === 0 ? "disabled" : "";
        };


        /**
         * @name nextPage
         * @desciption get next page.
         */
        vm.nextPage = function() {
            if ((vm.currentPage + 1) < vm.pageCount()) {
                vm.currentPage++;
                $scope.appVm.currentPage = vm.currentPage;
            }
        };

        /**
         * @name nextPageDisabled
         * @description disable nex page.
         * @returns {string}
         */
        vm.nextPageDisabled = function() {
            return (vm.currentPage + 1)  === vm.pageCount() ? "disabled" : "";
        };

        /**
         * @name setPage
         * @description change current page.
         * @param n
         */
        vm.setPage = function(n) {
            vm.currentPage = n;
            $scope.appVm.currentPage = vm.currentPage;
        };

        /**
         * @name shouldPaginate
         * @description determines if should show pagination.
         * @returns {boolean}
         */
        vm.shouldPaginate = function() {
            return vm.pageCount() > 1;
        };
    }
})();