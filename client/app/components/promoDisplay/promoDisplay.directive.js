(function(){
    'use strict';
    angular.module('promoDisplay')
        .directive('promoDisplay', PromoDisplayDirective);

    /**
     * @name                PromoDisplayDirective
     * @description         Promo display directive.
     * @returns {*}
     */
    function PromoDisplayDirective(){
        return {
            restrict: 'E',
            templateUrl: 'components/promoDisplay/promoDisplay.template.html'
        };
    }
})();
