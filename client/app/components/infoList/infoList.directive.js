(function(){
    'use strict';
    angular.module('infoList')
        .directive('infoList', InfoListDirective);

    /**
     * @name                   InfoListDirective
     * @description            Info list directive.
     * @returns {*}
     */
    function InfoListDirective(){
        return {
            restrict: 'E',
            templateUrl: 'components/infoList/infoList.template.html'
        };
    }
})();
