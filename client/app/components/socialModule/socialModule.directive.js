(function(){
    'use strict';
    angular.module('socialModule')
        .directive('socialModule', SocialModuleDirective);

    /**
     * @name                     SocialModuleDirective
     * @description              social module directive.
     * @returns {*}
     */
    function SocialModuleDirective(){
        return {
            restrict: 'E',
            templateUrl: 'components/socialModule/socialModule.template.html'
        };
    }
})();