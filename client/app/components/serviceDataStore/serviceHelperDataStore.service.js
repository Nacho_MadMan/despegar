(function(){
    'use strict';
    angular.module('serviceDataStore')
        .service('ServiceHelperDataStore', ServiceHelperDataStore);

    /**
     * @name                  ServiceHelperDataStore
     * @description           Singleton that stores the original service response and holds any helper method
     *                        to precess the data (in case they are needed).
     * @returns {*}
     */
    function ServiceHelperDataStore(){

        /**
         * @name              data
         * @description       Stores the service response to be used outside main controller without doing another
         *                    http request.
         * type {object}
         */
        var data;

        return {
            setData: setData,
            getData: getData
        };

        /**
         * @name              setData
         * @description       Sets data value.
         * @param newData
         */
        function setData(newData){
            data = newData;
        }

        /**
         * @name              getData
         * @description       Gets data value.
         * @returns {*}
         */
        function getData(){
            return data;
        }
    }
})();