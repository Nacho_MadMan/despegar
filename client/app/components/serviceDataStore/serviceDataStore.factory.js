(function() {
    'use strict';
    angular.module('serviceDataStore')
        .factory('ServiceDataStoreFactory', ServiceDataStoreFactory);

    ServiceDataStoreFactory.$inject = ['$http'];

    /**
     * @name                       ServiceDataStoreFactory
     * @description                Service Data store.
     * @param $http                http provider
     * @returns {*}
     */
    function ServiceDataStoreFactory($http) {
        /**
         * @var  hotels           Stores in memory the hotels response.
         * @type {null}
         */
        var hotels = null;

        /**
         * @var  lists            Stores in memory the lists response.
         * @type {null}
         */
        var lists = null;

        /**
         * @var comments          Stores in memory the comments response
         * @type {null}
         */
        var comments = null;

        return {
            getHotels: getHotels,
            getLists: getLists,
            getComments: getComments
        };

        /**}
         * @name                  getHotels
         * @description           get hotels response data.
         * @returns {*}
         */
        function getHotels() {
            if (hotels === null) {
                hotels = $http.get('media/mockData/staticData.json').then(function(res) {
                    return res.data.hotels;
                });
            }
            return hotels;
        }

        /**
         * @name                  getLists
         * @description           get list data response.
         * @returns {*}
         */
        function getLists() {
            if (lists === null) {
                lists = $http.get('media/mockData/staticDataList.json').then(function(res) {
                    return res.data;
                });
            }
            return lists;
        }

        /**
         * @name                  getLists
         * @description           get list data response.
         * @returns {*}
         */
        function getComments() {
            if (comments === null) {
                comments = $http.get('media/mockData/commentBubble.json').then(function(res) {
                    return res.data.commentBubble;
                });
            }
            return comments;
        }
    }
})();