(function() {
    'use strict';
    angular.module('filters')
        .filter('offset', OffsetFilter);

    /**
     * @name OffsetFilter
     * @description Applies offset filter.
     * @returns {Function}
     */
    function OffsetFilter() {
        return function(input, start) {
            if (typeof input !== 'undefined') {
                start = parseInt(start, 10);
                return input.slice(start);
            }
        };
    }
})();