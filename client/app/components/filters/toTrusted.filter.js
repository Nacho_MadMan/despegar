(function() {
    'use strict';
    angular.module('filters')
        .filter('toTrusted', ToTrustedFilter);

    ToTrustedFilter.$inject = ['$sce'];
    /**
     * @name           ToTrustedFilter
     * @description    Applies trusted filter to string to apply as html
     * @param $sce     $sce provider
     * @returns {Function}
     */
    function ToTrustedFilter($sce) {
        return function(text) {
            return $sce.trustAsHtml(text);
        };
    }
})();