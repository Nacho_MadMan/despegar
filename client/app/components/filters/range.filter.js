(function(){
    'use strict';
    angular.module('filters')
        .filter('range', RangeFilter);

    /**
     * @name                range
     * @description         custom filter
     * @returns {Function}
     */
    function RangeFilter(){
        return function(val, range) {
            range = parseInt(range);
            for (var i=0; i<range; i++){
                val.push(i);
            }
            return val;
        };
    }
})();
