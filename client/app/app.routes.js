(function() {
    'use strict';
    angular.module('despegarApp')
        .config(Router);
    /**
     * @name                        Router
     * @description                 Main Route config for the app
     *                              the main reason to use this was to resolve all the async service calls at once
     *                              injecting the result of the resolved promise into the main controller, saving
     *                              another http request.
     * @param $stateProvider        State provider
     * @param $urlRouterProvider    Url provider
     */
    function Router($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/");
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'app.template.html',
                resolve: {
                    hotels: function(ServiceDataStoreFactory) {
                        return  ServiceDataStoreFactory.getHotels();
                    },
                    lists: function(ServiceDataStoreFactory){
                        return ServiceDataStoreFactory.getLists();
                    },
                    comments: function(ServiceDataStoreFactory){
                        return ServiceDataStoreFactory.getComments();
                    }
                },
                controller: 'AppController',
                controllerAs: 'appVm'
            });
    }
})();