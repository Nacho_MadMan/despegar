author :    Aldama Juan Ignacio
            Examen front end para despegar.com
mail:       juan.choy@gmail.com
repository: https://bitbucket.org/Nacho_MadMan/despegar.git

   app deployment :
   ######################################################
   #                    NODE SERVER                     #
   ######################################################

   To run the server if node installed
      just run npm install to install node_modules
      if grunt is installed:
         run server with task "serve" ( grunt serve )
      else: just run "node ./server/app.js" to run server.

   ######################################################
   #                       OR                           #
   ######################################################

   Or just copy the "client" folder into yor web server

   ######################################################
   #                     Class map                      #
   ######################################################
      There is no class map, just a representation of the dependency injection and the function names.
      http://creately.com/diagram/ia3kay5k1/vRFoqWFzTaOm6n72Z19g8TDif0M%3D