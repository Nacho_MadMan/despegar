var express = require('express');
var path = require('path');
var http = require('http');
var dist = '../client/app';
var libs = '../client/libs';
// start express
var app = express();
var port = '8181';
var server;

// serve static content (angular app)
app.use('/', express.static(path.join(__dirname, dist)));
app.use('/libs', express.static(path.join(__dirname, libs)));
app.set('port', port);

/**
 * HTTP server.
 */
server = http.createServer(app);
server.listen(port);
server.on('error', function(error){
    console.log(error);
});
server.on('listening', function(){
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    console.log('Listening on ' + bind);
});